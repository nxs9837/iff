#' IFF
#'
#' A collection of utility tools for IFF theme/branding. For gernal use across 
#' different projects rather than specific to one use case.
#' 
#' 
#' GitHub: \url{https://github.com/nspyrison/cheem}
#' 
#' @name IFF
#' @docType package
#' @keywords internal
"_PACKAGE"

## usethis namespace: start
## Exports ------
#' @importFrom magrittr %>%
#' @export
magrittr::`%>%`
NULL ## Required for roxygen2 to work, do not delete
## usethis namespace: end
NULL

## Print message -----
#### prints upon first attaching the package
.onAttach <- function(...){
  packageStartupMessage("IFF --- version ", utils::packageVersion("IFF"))
}

## global variables -----
globalVariables(c(
  "iff_pal", "iff_pal_stagger"
))


