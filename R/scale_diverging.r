#' @name scale_diverging
#' @aliases scale_color_diverging
#' @aliases scale_fill_diverging
#'
#' @title Scale Color/Fill divergingly
#'
#' @description This function scales the color/fill of a ggplot object divergingly.
#' Pipe to this function rather than use the `+` operator.
#'
#' @param ggplot A ggplot object.
#' @param tgt_colors A vector of target colors to use for the color ramp. Default is c("white", "#0075cf").
#' @param seq_mix The minimum value for the color sequence. Default is 0.
#' @param seq_max The maximum value for the color sequence. Default is 1.
#'
#' @return A ggplot object with the color scale manually set.
#' @examples
#' library(IFF)
#' library(ggplot2)
#' 
#' # scale_COLOR_diverging
#' ## Create a ggplot
#' p <- mtcars %>%
#'   ggplot(aes(disp, mpg, color=factor(cyl))) +
#'   geom_point()
#' 
#' ## Default
#' p %>% scale_color_diverging()
#' 
#' ## Inverted custom scale
#' p %>% scale_color_diverging(
#'   tgt_colors = c("red", "white"), 0, .6
#' )
#' 
#' # scale_FILL_diverging
#' ## Create a ggplot
#' p <- mtcars %>%
#'   ggplot(aes(disp, mpg, fill = factor(cyl))) +
#'   geom_point(shape = 21, size = 2, color = "black")
#' 
#' ## Default
#' p %>% scale_fill_diverging()
#' 
#' ## Inverted custom scale
#' p %>% scale_fill_diverging(
#'   tgt_colors = c("red", "white"), 0, .6
#' )
NULL

#' @rdname scale_diverging
#' @export
scale_color_diverging <- function(
  ggplot, tgt_colors = c("blue", "grey60", "red"), seq_mix = 0, seq_max = 1
){
  ## Init
  ldat <- ggplot2::layer_data(ggplot)
  n_cols <- ldat$colour[!is.na(ldat$x)] %>%
    unique() %>% stats::na.omit() %>% length()
  ramp <- scales::colour_ramp(tgt_colors)
  pal <- ramp(seq(seq_mix, seq_max, length = n_cols))
  #show_col(pal)
  
  ggplot +
    ggplot2::scale_color_manual(values = pal)
}


#' @rdname scale_diverging
#' @export
scale_fill_diverging <- function(
    ggplot, tgt_colors = c("blue", "grey60", "red"), seq_mix = 0, seq_max = 1
){
  ## Init
  ldat <- ggplot2::layer_data(ggplot)
  n_cols <- ldat$fill[!is.na(ldat$x)] %>%
    unique() %>% stats::na.omit() %>% length()
  ramp <- scales::colour_ramp(tgt_colors)
  pal <- ramp(seq(seq_mix, seq_max, length = n_cols))
  #show_col(pal)
  
  ggplot +
    ggplot2::scale_fill_manual(values = pal)
}

