#' @title Append The IFF logo to a ggplot
#' @description This function appends a text message below a ggplot. 
#' The text message is centered and can be customized. 
#' Pipe a ggplot object rather than the `+` operator.
#' @param plot A ggplot object to which the text message will be appended.
#' @param logo_url The URL of a logo. Defaults to transparent IFF logo.
#' @param rel_width The relative width of the logo, as a fraction of the figure.
#' @param rel_height The relative height of the logo, Actually the ymin of the 
#' annotation, so you need to "pull down" twice as far as you expect.
#' @return A ggplot object with the text message appended.
#' @export
#' @examples 
#' library(IFF)
#' library(ggplot2)
#' 
#' ## Base plot
#' p <- mtcars %>%
#'   ggplot(aes(disp, mpg, color=factor(cyl))) +
#'   geom_point()
#' 
#' \dontrun{
#' ## Default IFF Logo
#' p %>% append_plot_logo()
#' 
#' ## Customized logo
#' p %>% append_plot_logo(
#' "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg",
#' rel_width = 2.5, rel_height = -.9)}
append_plot_logo <- function(
    plot,
    logo_url = "https://www.iff.com/sites/iff-corp/files/iff/iff-logo.png",
    rel_width = .2,
    rel_height = .9
){
  # Create a plot with the logo
  logo_img <- magick::image_read(logo_url)
  logo_plot <-  ggplot2::ggplot() +
    ggplot2::theme_void() +
    ggplot2::annotation_custom(
      grid::rasterGrob(logo_img),
      xmin=-Inf, xmax=Inf, ymin=rel_height, ymax=Inf
    )
  
  # Combine the plots
  logo_plot + plot +
    patchwork::plot_layout(widths = c(rel_width, 1))
}

