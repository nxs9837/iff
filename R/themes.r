#' @name themes
#' @aliases theme_vlegend
#' @aliases theme_ns
#' 
#' @title Ggplot Themes
#' 
#' @description Returns a ggproto, a list of ggplot theme settings to change 
#' the theme of a ggplot object.
#'
#' @return A ggproto that can be used to change the theme of a ggplot.
#' @examples
#' library(IFF)
#' library(ggplot2)
#' 
#' gg <- ggplot(mtcars, aes(disp, mpg, color = factor(cyl))) + geom_point()
#' 
#' gg
#' gg + theme_vlegend()
#' gg + theme_ns()
#' gg + theme_ns() + theme_vlegend()
NULL


#' @rdname themes
#' @export
theme_vlegend <- function() {
  list(ggplot2::theme(legend.position="bottom"))
}
#' @rdname themes
#' @export
theme_ns <- function() {
  list(ggplot2::theme_bw(18),
       ggplot2::scale_color_brewer(palette = "Dark2"),
       ggplot2::scale_fill_brewer(palette = "Dark2")
  )
}
