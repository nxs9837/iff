#' Open .Rprofile
#'
#' This function opens the .Rprofile file in your home directory for editing.
#'
#' @return No return value, called for side effects
#' @export
#' @examples
#' \dontrun{
#' library(IFF)
#' open_rprofile()
#' }
open_rprofile <- function() utils::file.edit(file.path("~", ".Rprofile"))

#' Open .Renviron
#'
#' This function opens the .Renviron file in your home directory for editing.
#'
#' @return No return value, called for side effects
#' @export
#' @examples
#' \dontrun{
#' library(IFF)
#' open_renviron()
#' }
open_renviron <- function() utils::file.edit(file.path("~", ".Renviron"))



#' Custom head function
#'
#' This function prints the first 'n' elements of an object. If the object is a 'tbl_df', 
#' it prints with a specified width. For other objects, it temporarily changes the 'max.print' 
#' option to 'n' for printing.
#'
#' @param obj An object to print.
#' @param n Number of elements to print. Default is Inf.
#' @param width Width to use when printing a 'tbl_df'. Default is Inf.
#' @return No return value, called for side effects.
#' @export
#' @examples
#' \dontrun{
#' library(IFF)
#' head(mtcars, 5)
#' }
head <- function(obj, n, width = Inf) {
  if (class(obj)[1] == "tbl_df") {
    print(obj, width = width, n = n)
  }
  else {
    prev_max_print <- getOption("max.print")
    options(max.print = n)
    print(obj)
    options(max.print = prev_max_print)
  }
}



#' Count Lines of R Code in a Repository
#'
#' This function counts the number of lines of R and Rmd files in a given repository.
#' It returns the total number of files, total lines, and the mean lines per file.
#'
#' @param repo_path A string. The path to the repository. Default is current directory.
#' @param tgt_ext A named character vector. The file extensions to count lines for. See the Usage section for the default.
#' @return No return value, called for side effects.
#' @export
#' @examples
#' \dontrun{
#' library(IFF)
#' count_repo_r_lines(repo_path = ".", tgt_ext = c(r=".[Rr]$", rmd=".[Rr][Mm][Dd]$"))
#' }
count_repo_r_lines <- function(
    repo_path = ".",
    tgt_ext = c(r=".[Rr]$", rmd=".[Rr][Mm][Dd]$")
) {
  files <- list.files(path = repo_path, recursive = TRUE, full.names = TRUE)
  tgt_files <- NULL
  for(i in 1:length(tgt_ext)){
    tgt_files <- append(
      tgt_files,
      stringr::str_subset(files, tgt_ext[i])
    )
  }
  file_lines <- sapply(tgt_files, function(x) x %>% 
                         readLines() %>% length())
  message(paste0(length(file_lines), " files contained ", 
                 sum(file_lines), " lines combined. A mean of ",
                 round(sum(file_lines) / length(file_lines), 2), 
                 " lines per file."))
}



#' Undebug All Namespaces
#'
#' This function undebugs all namespaces in the given environment.
#' It checks if a function is debugged and if so, it undebugs it.
#' It works for both regular environments and namespaces.
#'
#' @param where A character vector. The search path to look for debugged functions. Default is the current search path.
#' @return No return value, called for side effects.
#' @export
#' @examples
#' \dontrun{
#' library(IFF)
#' undebug_namespaces()
#' }
undebug_namespaces <- function(where = search()) {
  isdebugged_safe <- function(x, ns = NULL) {
    g <- if (is.null(ns)) 
      get(x)
    else utils::getFromNamespace(x, ns)
    is.function(g) && isdebugged(g)
  }
  which_debugged <- function(objnames, ns = NULL) {
    if (!length(objnames)) 
      return(character(0L))
    objnames[sapply(objnames, isdebugged_safe, ns = ns)]
  }
  all_debugged <- function(where = search(), show_empty = FALSE) {
    ap <- stats::setNames(lapply(where, function(x) {
      which_debugged(ls(x, all.names = TRUE))
    }), gsub("package:", "", where))
    ns <- unlist(sapply(gsub("package:", "", where), 
                        function(x) {
                          if (inherits({
                            n <- try(getNamespace(x), silent = TRUE)
                          }, "try-error")) 
                            NULL
                          else x
                        }))
    ap_ns <- stats::setNames(lapply(ns, function(x) {
      objects <- ls(getNamespace(x), all.names = TRUE)
      which_debugged(objects, ns = x)
    }), ns)
    if (!show_empty) {
      ap <- ap[sapply(ap, length) > 0L]
      ap_ns <- ap_ns[sapply(ap_ns, length) > 0L]
    }
    for (i in names(ap)) ap_ns[[i]] <- setdiff(ap_ns[[i]], 
                                               ap[[i]])
    list(env = ap, ns = ap_ns)
  }
  aa <- all_debugged(where)
  lapply(aa$env, undebug)
  invisible(mapply(function(ns, fun) {
    undebug(utils::getFromNamespace(fun, ns))
  }, names(aa$ns), aa$ns))
}

