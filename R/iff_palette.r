## iff_pal -----
#' IFF Palette
#' 
#' A table of IFF brand colors and various ways to describe the colors.
#' 
#' @format  A complete tibble with 10 observations and 5 character variables,
#' name of the colors and four ways to describe them.
#' \itemize{
#'   \item Name, the name of the color
#'   \item Pantone, string describing the the color
#'   \item CMYK, string describing the the color
#'   \item RGB, string describing the the color
#'   \item HEX, string describing the the color
#' }
#' @details
#' __Replicating this dataset:__
#' ```
#' library(dplyr)
#' library(readxl)
#' 
#' IFFcolors <- readxl::read_excel("~/IFFcolors.xlsx")
#' iff_pal <- IFFcolors[1:10,] %>% select(
#'   name=Name, hex=HEX, rgb=RGB, cmyk=CMYK, pantone=Pantone)
#' 
#' ## Manually constructed staggering
#' idx <- c(1, 9, 7, 10, 5, 3)
#' iff_pal_stagger <- iff_pal[idx,]
#' 
#' if(F){ ## Don't accidentally save
#'   save(iff_pal, file = "./data/iff_pal.rda")
#'   save(iff_pal_stagger, file = "./data/iff_pal_stagger.rda")
#'   usethis::use_data(iff_pal)
#'   usethis::use_data(iff_pal_stagger)
#' }
#' ```
#' @name iff_pal
#' @docType data
#' @source {Jennifer Knack, manually curated}
#' @examples
#' library(IFF)
#' library(ggplot2)
#' 
#' iff_pal
#' 
#' ## iff_pal_stagger tried to stagger colors for better human perception
#' ## of data visualization, not as good as the Dark2 palette.
#' p <- diamonds[1:100,] %>%
#'   ggplot(aes(depth, table, color=color)) +
#'   geom_jitter()
#'   
#' p %>% scale_color_categorical(iff_pal_stagger$hex)
"iff_pal"

#' @rdname iff_pal
"iff_pal_stagger"

