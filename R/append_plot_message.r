#' @title Append a Text Message to a ggplot
#' @description This function appends a text message below a ggplot. 
#' The text message is centered and can be customized. 
#' Pipe a ggplot object rather than the `+` operator.
#' @param plot A ggplot object to which the text message will be appended.
#' @param rel_height A numeric value indicating the relative height of the text message in the final plot. Default is 0.05.
#' @param text A character string specifying the text message to be appended. Default is "IFF Confidential".
#' @param size A numeric value indicating the size of the text. Default is 3.
#' @param color A character string specifying the color of the text. Default is "grey60".
#' @return A ggplot object with the text message appended.
#' @export
#' @examples 
#' library(IFF)
#' library(ggplot2)
#' 
#' ## Base plot
#' p <- mtcars %>%
#'   ggplot(aes(disp, mpg, color=factor(cyl))) +
#'   geom_point()
#' 
#' \dontrun{
#' ## Default IFF Confidential
#' p %>% append_plot_message()
#' 
#' ## Customized message
#' p %>% append_plot_message(.3, "Custom text", 5, "red")}
append_plot_message <- function(
    plot,
    rel_height = .05,
    text = "IFF Confidential",
    size = 3,
    color = "grey60"
){
  # Create a plot with 'confidential' text
  text_plot <- ggplot2::ggplot() +
    ggplot2::annotate("text", x = 0.5, y = 0.5, label = text, 
             hjust = 0.5, vjust = .5, size = size, color = color) +
    ggplot2::theme_void() +
    ggplot2::theme(plot.margin = ggplot2::margin(0, 0, 0, 0, "pt"))
  
  # Combine the plots
  final_plot <- plot / text_plot +
    patchwork::plot_layout(heights = c(1, rel_height))
  
  final_plot
}




