#' @name scale_categorical
#' @aliases scale_color_categorical
#' @aliases scale_fill_categorical
#'
#' @title Scale Color/Fill categorically
#'
#' @description This allows for more manual control of color and fill colors, 
#' but first drafts should use more color blind-safe palettes!
#' This function scales the color/fill of a ggplot object categorically.
#' Pipe to this function rather than use the `+` operator.
#'
#' @param ggplot A ggplot object.
#' @param tgt_colors A vector of target colors to use for the color ramp. Default is c("white", "#0075cf").
#'
#' @return A ggplot object with the color scale manually set.
#' @examples
#' library(IFF)
#' library(ggplot2)
#' 
#' # scale_COLOR_categorical
#' ## Create a ggplot
#' p <- diamonds[1:100,] %>%
#'   ggplot(aes(depth, table, color = color)) +
#'   geom_jitter()
#'   
#' ## First check your favorite color-blind safe palette
#' p + scale_color_brewer(palette = "Dark2")
#' 
#' ## Default
#' p %>% scale_color_categorical()
#' 
#' ## Inverted custom scale
#' p %>% scale_color_categorical(c("black", "white", "red"))
#' 
#' # scale_FILL_categorical
#' ## Create a ggplot
#' p <- diamonds[1:100,] %>%
#'   ggplot(aes(depth, table, fill = color)) +
#'   geom_jitter(shape = 21, size = 3, color = "black")
#' 
#' ## First check your favorite color-blind safe palette
#' p + scale_fill_brewer(palette = "Dark2")
#' 
#' ## Default
#' p %>% scale_fill_categorical()
#' 
#' ## Inverted custom scale
#' p %>% scale_fill_categorical(c("black", "white", "red"))
NULL

#' @rdname scale_categorical
#' @export
scale_color_categorical <- function(
  ggplot, tgt_colors = iff_pal_stagger$hex
){
  ## Init
  ldat <- ggplot2::layer_data(ggplot)
  n_cols <- ldat$colour[!is.na(ldat$x)] %>%
    unique() %>% stats::na.omit() %>% length()
  pal <- tgt_colors[1:n_cols]
  #show_col(pal)
  
  ggplot +
    ggplot2::scale_color_manual(values = pal)
}


#' @rdname scale_categorical
#' @export
scale_fill_categorical <- function(
    ggplot, tgt_colors = iff_pal_stagger$hex
){
  ## Init
  ldat <- ggplot2::layer_data(ggplot)
  n_cols <- ldat$fill[!is.na(ldat$x)] %>%
    unique() %>% stats::na.omit() %>% length()
  pal <- tgt_colors[1:n_cols]
  #show_col(pal)
  
  ggplot +
    ggplot2::scale_fill_manual(values = pal)
}

