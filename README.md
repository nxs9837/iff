Nicholas Spyrison
January 2024

# IFF

An R package for IFF themes, palettes, and templates

# Installation

Nick needs to manually provide permission to the GitLab repository, then IFF can be installed with:

`devtools::install_gitlab("nxs9837/iff")`